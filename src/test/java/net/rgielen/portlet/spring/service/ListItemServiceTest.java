package net.rgielen.portlet.spring.service;

import static org.junit.Assert.*;

import javax.inject.Inject;

import net.rgielen.portlet.spring.entity.ListItem;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:core-context.xml")
@Transactional
public class ListItemServiceTest {

	@Inject ListItemService listItemService;
	
	@Test
	public void testServiceIsCreatable() throws Exception {
		assertNotNull(listItemService);
	}
	
	@Test
	public void testItemCanSaved() throws Exception {
		ListItem foo = new ListItem("foo", 100);
		listItemService.saveOrUpdate(foo);
		assertNotNull(foo.getId());
		assertNotNull(listItemService.get(foo.getId()));
	}	
	
}
