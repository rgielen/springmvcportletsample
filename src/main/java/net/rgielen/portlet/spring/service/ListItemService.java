package net.rgielen.portlet.spring.service;

import javax.inject.Named;

import org.springframework.transaction.annotation.Transactional;

import net.rgielen.portlet.spring.entity.ListItem;

@Named
@Transactional
public class ListItemService extends GenericEntityService<ListItem, Integer>{

	@Override
	protected Class<ListItem> entityClass() {
		return ListItem.class;
	}

}
