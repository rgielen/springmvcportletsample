package net.rgielen.portlet.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * ListItem.
 *
 * @author Rene Gielen
 */
@Entity
@SequenceGenerator(name = "id_seq", allocationSize=1)
public class ListItem {

	private Integer id;
	private String name;
	private Integer amount;
	
	public ListItem() {
	}
		
	public ListItem(String name, Integer amount) {
		super();
		this.name = name;
		this.amount = amount;
	}

	@Id
	@GeneratedValue(generator="id_seq")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(length=255)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
}
