package net.rgielen.portlet.spring.controller;

import java.util.Locale;

import javax.inject.Inject;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import net.rgielen.portlet.spring.entity.ListItem;
import net.rgielen.portlet.spring.service.ListItemService;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

@Controller
@RequestMapping("VIEW")
public class ListItemController {

	@Inject ListItemService listItemService;
	
	@RenderMapping
	public String renderLandingPage(Model model) {
		model.addAttribute("items", listItemService.findAll());
		return "landingPage";
	}
	
	@RenderMapping(params="view=editItem")
	public String renderEditItemPage(@RequestParam Integer id, Model model) {
		ListItem item = listItemService.get(id);
		model.addAttribute("listItem", item);
		return "editItem";
	}
	
	@RenderMapping(params="view=newItem")
	public String renderNewItemPage(Model model) {
		model.addAttribute("listItem", new ListItem());
		return "editItem";
	}
	
	@ActionMapping("saveItem")
	public void saveItemAlternative( @ModelAttribute("listItem") ListItem listItem, Locale currentLocale, ActionResponse response) {
		listItemService.saveOrUpdate(listItem);
		response.setRenderParameter("id", ""+listItem.getId());
	}
	
	@ActionMapping(params="action=deleteItem")
	public void deleteItem(@RequestParam Integer id) {
		listItemService.delete(listItemService.get(id));
	}
	
	@ActionMapping(params="action=saveItemSimple")
	public void saveItem(ActionRequest request, ActionResponse response, 
			@RequestParam String name, @RequestParam Integer amount, Locale currentLocale) {
		listItemService.saveOrUpdate(new ListItem(name, amount));
	}
	
}
