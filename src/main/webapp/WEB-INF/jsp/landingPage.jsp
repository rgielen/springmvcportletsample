<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:actionURL var="saveItemUrl">
	<portlet:param name="action" value="saveItem"/>	
</portlet:actionURL>
<portlet:renderURL var="newItemUrl">
	<portlet:param name="view" value="newItem"/>	
</portlet:renderURL>
<h1>Hello</h1>

<c:forEach var="item" items="${items}">
	<portlet:renderURL var="editItemUrl">
		<portlet:param name="view" value="editItem"/>	
		<portlet:param name="id" value="${item.id}"/>	
	</portlet:renderURL>
    <li><a href="${editItemUrl}"><c:out value="${item.name}"/> - <c:out value="${item.amount}"/></a></li>
</c:forEach>
<p/>
<a href="${newItemUrl}">New Item</a>

