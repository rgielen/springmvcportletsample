<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:actionURL var="saveItemUrl" name="saveItem"/>
<portlet:actionURL var="saveItemAlternativeUrl">
	<portlet:param name="action" value="saveItemAlternative"/>	
</portlet:actionURL>
<portlet:actionURL var="deleteItemUrl">
	<portlet:param name="action" value="deleteItem"/>	
	<portlet:param name="id" value="${listItem.id}"/>	
</portlet:actionURL>

<h1>Edit Item</h1>
<form:form method="post" modelAttribute="listItem" action="${saveItemUrl}">
	<form:hidden path="id"/>
	<form:label path="name">Name</form:label>
	<form:input path="name"/>
	<form:label path="name">Amount</form:label>
	<form:input path="amount"/>
	<input type="submit" value="OK"/>
</form:form>
<a href='<portlet:renderURL/>'>Back to list</a>
<c:if test="${not empty listItem.id}">
- <a href="${deleteItemUrl}">Delete Item</a> 
</c:if>
